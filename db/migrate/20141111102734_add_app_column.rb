class AddAppColumn < ActiveRecord::Migration
  def change
    add_column :apps, :google_url, :string
    add_column :apps, :apple_url, :string
    add_column :apps, :url, :string
    add_column :apps, :status, :bool


  end
end

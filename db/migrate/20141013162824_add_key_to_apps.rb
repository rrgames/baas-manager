class AddKeyToApps < ActiveRecord::Migration
  def change
    add_column :apps, :app_key, :string
    add_column :apps, :app_secret, :string
  end
end

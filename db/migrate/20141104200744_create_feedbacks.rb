class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
       t.timestamps
    end
    add_column :feedbacks, :user_id, :integer
    add_column :feedbacks, :data, :string

  end
end

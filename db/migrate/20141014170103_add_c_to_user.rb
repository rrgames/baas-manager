class AddCToUser < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_column :users, :password, :string
    add_column :users, :name, :string
    add_column :users, :email, :string
    add_column :users, :contact, :string
    add_column :users, :api_key, :string

  end
end

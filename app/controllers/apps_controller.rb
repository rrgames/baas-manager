class AppsController < ActionController::Base
  before_action :vaildate_session
  before_action :set_app
  skip_before_action :verify_authenticity_token

  require 'securerandom'
  require 'digest/md5'

  # GET /apps
   def index
     @query_count = {}
     @user_count = {}

    if @apps.size <= 0
      redirect_to "/@#{@user.username}/apps/new"
      return
    end
     @apps.each do |app|
        url = URI.parse("https://rankaas.io:4785/#{@user.api_key}/#{app.app_key}/application/query")
        http = Net::HTTP.new(url.host, url.port)
	      http.use_ssl = true
        request = Net::HTTP::Get.new(url.path)
        request["Authorization"] = "Bearer " + app.app_secret

        response = http.request(request)
        @data = ActiveSupport::JSON.decode(response.body)
        @query_count[app.name] = @data["count"].to_i
        @user_count[app.name] = @data["user_count"].to_i
     end
   end

   def showlog
     if params[:app_key]
       url = URI.parse("https://rankaas.io:4785/#{@user.api_key}/#{params[:app_key]}/application/log")
    else
       url = URI.parse("https://rankaas.io:4785/#{@user.api_key}/#{@apps[0].app_key}/application/log")
     end
     http = Net::HTTP.new(url.host, url.port)
     request = Net::HTTP::Get.new(url.path)
     http.use_ssl = true
     response = http.request(request)
     @data = ActiveSupport::JSON.decode(response.body)
     render json: @data
   end

   def feedback
     Feedback.create(user_id: @user.id, data: params[:data])
   end

  # GET /apps/name
   def show
  end

  def create
    @app = App.new(app_params)

    @app.app_key = SecureRandom.uuid
    @app.app_secret = Digest::MD5.hexdigest(SecureRandom.uuid)
    @app.user_id = session[:user_id]


    if (params[:app][:name] && !params[:app][:name].blank? && params[:app][:name].size > 3)
    App.where(user_id: @app.user_id).each do |app|
	if app.name == params[:app][:name]
		redirect_to "/@#{@user.username}/apps/new", :flash => { message: "애플리케이션의 이름은 중복될 수 없습니다." }
	return
	end
    end
      if @app.save
        url = URI.parse("https://rankaas.io:4785/#{@user.api_key}/#{@app.app_key}/createApp/#{@app.app_secret}")
        request = Net::HTTP::Get.new(url.to_s)
        res = Net::HTTP.new(url.host, url.port)

        res.use_ssl = true

        @data = res.request(request)

        redirect_to "/@#{@user.username}/apps/#{@app.name}/"
     end
   else
	redirect_to "/@#{@user.username}/apps/new", :flash => { message: "애플리케이션의 이름을 알맞게 입력해주세요" }
    end
   end

  # GET /apps/new
  def new
    @app = App.new
  end

  def update
    if @app.update(app_params)
      redirect_to "/@#{@user.username}/apps/#{@app.name}/edit"
      return
    end
    redirect_to "/@#{@user.username}/apps/#{@app.name}/edit"
  end

  # GET /apps/1/edit
  def edit

  end

  private

    def vaildate_session
      if session[:user_id]
        @user = User.find_by(id: session[:user_id])

        if not @user
          redirect_to :login, :flash => { message: "잘못된 접근입니다." }
        else
          if @user.username != params[:username]
            session[:user_id] = nil
            redirect_to :login, :flash => { message: "잘못된 접근입니다." }
          end
        end
      else
        redirect_to :login, :flash => { message: "잘못된 접근입니다." }
      end
    end

    def set_app
      @apps = App.where(user_id: @user.id)
      @app = @apps.find_by(name: params[:id])
     end

    # Never trust parameters from the scary internet, only allow the white list through.
    def app_params
      params.require(:app).permit(:name, :description, :url, :google_url, :apple_url)

    end
end

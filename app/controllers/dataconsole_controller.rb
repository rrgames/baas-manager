require 'net/http'

class DataconsoleController < ActionController::Base
  before_action :vaildate_session
  skip_before_action :verify_authenticity_token

  def index

  end

  def httpRequest(type, url, data = nil)

    url = URI.parse("https://rankaas.io:4785/#{@user.api_key}/#{@app.app_key}/" + url)
    http = Net::HTTP.new(url.host, url.port)
    if type == "GET"
      request = Net::HTTP::Get.new(url.to_s)
    elsif type == "POST"
      request = Net::HTTP::Post.new(url.path, {'Content-Type' =>'application/x-www-form-urlencoded'})
    elsif type == "DELETE"
      request = Net::HTTP::Delete.new(url.to_s)
    elsif type == "PATCH"
      request = Net::HTTP::Patch.new(url.path, {'Content-Type' =>'application/x-www-form-urlencoded'})
    end
    http.use_ssl = true
    request["Authorization"] = "Bearer " + @app.app_secret
    request.body = data if data != nil

    response = http.request(request)
    @data = ActiveSupport::JSON.decode(response.body)
    return @data
  end


  def createScheme
    @data = httpRequest("POST", "#{params[:collectionName]}/scheme", "insert_data=" + params[:insert_data])
     render json: @data
  end

  def updateScheme
    @data = httpRequest("PATCH", "#{params[:collection]}/scheme", "update_data=" + params[:update_data])
    render json: @data
  end

  def indexScheme
    @data = httpRequest("GET", "collections/")
    render json: @data
  end

  def deleteScheme
    @data = httpRequest("DELETE", "#{params[:collection]}/scheme")
    render json: @data
  end

  def deleteObject
    @data = httpRequest("DELETE", "#{params[:collection]}/#{params[:objectId]}")
    #@data = ActiveSupport::JSON.decode(res.body)
    render json: "{}"
  end

  def showObject
    @data = httpRequest("GET", "#{params[:collection]}/#{params[:objectId]}")
    render json: @data
  end

  def updateObject
    @data = httpRequest("PATCH", "#{params[:collection]}/#{params[:objectId]}", "update_data=" + params[:update_data])
    #@data = ActiveSupport::JSON.decode(res.body)
    render json: "{}"
  end

  def indexData
    if params[:scheme] == "true"
      @data = httpRequest("GET", "#{params[:collection]}/scheme")
    else
      if params[:collection] == "ranks"
        @data = httpRequest("GET", "#{params[:collection]}/?n=all")
      else
      @data = httpRequest("GET", "#{params[:collection]}/?offset=#{params[:offset]}")
      end
    end
    render json: @data
  end

  def createData
    @data = httpRequest("POST", "#{params[:collection]}/", "insert_data=" + params[:insert_data])
    render json: @data
  end

  def apiExecute

    method = params[:method]
    url = params[:url]
    token = params[:token]
    body = params[:body] ? params[:body] : ""

    if method == "PATCH"
      body = "update_data=" + body
    elsif method == "POST"
      body = "insert_data=" + body
    end

    url = URI.parse("https://rankaas.io:4785/#{@user.api_key}/#{@app.app_key}" + url)
    http = Net::HTTP.new(url.host, url.port)
    if type == "GET"
      request = Net::HTTP::Get.new(url.to_s)
    elsif type == "POST"
      request = Net::HTTP::Post.new(url.path, {'Content-Type' =>'application/x-www-form-urlencoded'})
    elsif type == "DELETE"
      request = Net::HTTP::Delete.new(url.to_s)
    elsif type == "PATCH"
      request = Net::HTTP::Patch.new(url.path, {'Content-Type' =>'application/x-www-form-urlencoded'})
    end
    http.use_ssl = true
    request["Authorization"] = "Bearer " + (token == "{App Secret}" ? @app.app_secret : token)
    request.body = body if body != ""

    response = http.request(request)
    return response.body
  end


  def vaildate_session
    if session[:user_id]
      @user = User.find_by(id: session[:user_id])

      if not @user
        redirect_to :login, :flash => { message: "잘못된 접근입니다." }
      else
        if @user.username != params[:username]
          session[:user_id] = nil
          redirect_to :login, :flash => { message: "잘못된 접근입니다." }
        else
          @apps = App.where(user_id: @user.id)
          @app = @apps.find_by(name: params[:app_id])
        end
      end
    else
      redirect_to :login, :flash => { message: "잘못된 접근입니다." }
    end
  end

end

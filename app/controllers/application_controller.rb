class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

   def show_nav
    @view_nav = true
  end
  def hide_nav
    @view_nav = false
  end

  def single_1st_page

  end
end

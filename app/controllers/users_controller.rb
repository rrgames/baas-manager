class UsersController < ActionController::Base
  before_action :vaildate_session, only: [:login, :register, :sessionCreate, :create]
  before_action :set_user, only: [:edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  require 'securerandom'

  def login

  end

  def register
  end

  def logout
    session[:user_id] = nil
    redirect_to "/"
   end

  def show
  end

  def sessionCreate
    formData = params[:user]
    if formData[:username] != nil and formData[:password] != nil

      if @user = User.authenticate(formData[:username], formData[:password])
        session[:user_id] = @user.id
        redirect_to "/@#{@user.username}/apps/"
        return
      end
    end
    redirect_to :login, :flash => { message: "Username or Password does not match." }
  end

  def edit

  end

  def create
    @user = User.new(user_params)
    @user.new_password = @user.password

    if User.find_by(username: @user.username) != nil
      redirect_to :register , :flash => { message: "중복된 아이디입니다" }
      return
    end
    if @user.username.size < 4
	redirect_to :register , :flash => { message: "id가 짧습니다." }
	return
    end
    if @user.password.size < 6
	redirect_to :register , :flash => { message: "패스워드가 짧습니다." }
	return
    end
    if @user.name.size < 2
	redirect_to :register , :flash => { message: "이름이 짧습니다." }
	return
    end
    if @user.contact.size != 11
      redirect_to :register , :flash => { message: " 연락처를 알맞게 입력해주세요. -(하이픈)은 포함하지 않습니다." }
      return
    end

    if @user.email.size < 2
      redirect_to :register , :flash => { message: "이메일을 알맞게 입력해주세요." }
      return
    end

    @user.api_key = SecureRandom.uuid
    if @user.save
      session[:user_id] = @user.id
      redirect_to "/@#{@user.username}/apps/"
    	return
    else
    	redirect_to :register , :flash => { message: "양식을 알맞게 채워주세요." }
    	return
    end
  end

  # PATCH/PUT /users/1
   def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
   def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
     end
  end

  private
    def set_user
      #@user = User.find(session[:user_id])
    end

    def vaildate_session
      if session[:user_id]
        @user = User.find_by(id: session[:user_id])

        if @user
          redirect_to "/@#{@user.username}/apps/"
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :email, :password, :contact, :name)

    end
end

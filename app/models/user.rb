require 'bcrypt'

class User < ActiveRecord::Base
  has_many :apps
  validates :name, presence: true
  before_save { self.email = email.downcase }
  validates :username, format: { with: /\A[a-zA-Z0-9]+\Z/ }
  validates :contact, :numericality => {:only_integer => true}
  attr_accessor :new_password
  before_save :hash_new_password, :if=>:password_changed?

  def password_changed?
    !@new_password.blank?
  end

  def hash_new_password
    self.password = BCrypt::Password.create(@new_password)
  end

  def self.authenticate(username, password)
    if user = User.find_by(username: username)
      if BCrypt::Password.new(user.password).is_password? password
        return user
      end
    end
    return nil
  end

end

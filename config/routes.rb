Rails.application.routes.draw do
  root :to => "home#index"
  get '/dev' => "home#dev"
get '/price' => "home#price"
get '/customers' => "home#customers"
get '/features' => "home#features"

get '/agreement' => "home#agreement"
get '/privacy' => "home#privacy"

  get '/login' => 'users#login'
  get '/register' => 'users#register'
  get '/logout' => 'users#logout'

   get '/@:id' => 'users#show'
   post '/users' => 'users#create'
   post '/session' => 'users#sessionCreate'

  delete '/@:id' => 'users#destory'

  get	'/@:username/apps'=> 'apps#index'
  post	'/@:username/apps' =>	'apps#create'

  get	'/@:username/apps/new' =>	'apps#new'
  get	'/@:username/apps/:id/edit' =>	'apps#edit'
  get	'/@:username/apps_log' =>	'apps#showlog'
  get	'/@:username/feedback' => 'apps#feedback'

  get	'/@:username/apps/:id' =>	'apps#show'

  patch '/@:username/apps/:id' => 'apps#update'
  put	'/@:username/apps/:id' =>	'apps#update'
  delete	'/@:username/apps/:id' =>	'apps#destroy'




  get	'/@:username/apps/:app_id/dataconsole'=> 'dataconsole#index'
  get	'/@:username/apps/:app_id/dataconsole/api'=> 'dataconsole#apiExecute'

  get '/@:username/apps/:app_id/dataconsole/collections'=> 'dataconsole#indexScheme'
  post '/@:username/apps/:app_id/dataconsole/collections/new'=> 'dataconsole#createScheme'

  get	'/@:username/apps/:app_id/dataconsole/collections/:collection'=> 'dataconsole#indexData'
  post	'/@:username/apps/:app_id/dataconsole/collections/:collection'=> 'dataconsole#createData'
  delete '/@:username/apps/:app_id/dataconsole/collections/:collection'=> 'dataconsole#deleteScheme'
  patch '/@:username/apps/:app_id/dataconsole/collections/:collection'=> 'dataconsole#updateScheme'

  get	'/@:username/apps/:app_id/dataconsole/collections/:collection/:objectId'=> 'dataconsole#showObject'

  delete '/@:username/apps/:app_id/dataconsole/collections/:collection/:objectId'=> 'dataconsole#deleteObject'
  patch '/@:username/apps/:app_id/dataconsole/collections/:collection/:objectId'=> 'dataconsole#updateObject'



 end

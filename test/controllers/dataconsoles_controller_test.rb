require 'test_helper'

class DataconsolesControllerTest < ActionController::TestCase
  setup do
    @dataconsole = dataconsoles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dataconsoles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dataconsole" do
    assert_difference('Dataconsole.count') do
      post :create, dataconsole: {  }
    end

    assert_redirected_to dataconsole_path(assigns(:dataconsole))
  end

  test "should show dataconsole" do
    get :show, id: @dataconsole
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dataconsole
    assert_response :success
  end

  test "should update dataconsole" do
    patch :update, id: @dataconsole, dataconsole: {  }
    assert_redirected_to dataconsole_path(assigns(:dataconsole))
  end

  test "should destroy dataconsole" do
    assert_difference('Dataconsole.count', -1) do
      delete :destroy, id: @dataconsole
    end

    assert_redirected_to dataconsoles_path
  end
end
